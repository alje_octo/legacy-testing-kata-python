import time


class TechBlogs(object):
    @staticmethod
    def list_all_blogs():
        time.sleep(5)  # access to DB is very long

        return {"HackerNews", "Reddit", "TechCrunch", "BuzzFeed",
                "TMZ", "TheHuffPost", "GigaOM"}