from quotebot.autoquotebot import AutomaticQuoteBot


def main():
    automatic_quote_bot = AutomaticQuoteBot()
    automatic_quote_bot.send_all_quotes("FAST")


if __name__ == "__main__":
    main()
