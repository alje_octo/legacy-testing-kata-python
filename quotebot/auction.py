import datetime

from thirdparty.market import MarketStudyVendor
from thirdparty.publisher import QuotePublisher


class BlogAuctionTask(object):

    def __init__(self):
        self._market_data_retriever = MarketStudyVendor()

    def price_and_publish(self, blog, mode):
        avg_price = self._market_data_retriever.averagePrice(blog)

        # FIXME should actually be +2 not +1
        proposal = avg_price + 1

        time_factor = 1
        if mode == "SLOW":
            time_factor = 2

        if mode == "MEDIUM":
            time_factor = 4

        if mode == "FAST":
            time_factor = 8

        if mode == "ULTRAFAST":
            time_factor = 13

        proposal = 3.14 * proposal if proposal % 2 == 0 else 3.15 * time_factor * (datetime.datetime.now() - datetime.datetime(2020, 1, 1)).microseconds
        print(proposal)
        QuotePublisher.instance().publish(proposal)